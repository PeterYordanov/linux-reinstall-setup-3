#!/bin/bash

echo "*-> Installing VeraCrypt... <-*"
add-apt-repository ppa:unit193/encryption
apt update -y
apt install -y veracrypt

echo "*-> Installing Discord... <-*"
snap install -y discord

echo "*-> Installing Microsoft Edge... <-*"
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
rm microsoft.gpg
apt update && apt install -y microsoft-edge-dev

echo "*-> Installing Firefox... <-*"
apt install -y firefox

echo "*-> Installing KeePassX... <-*"
apt install -y keepassx

echo "*-> Installing Notion... <-*"
snap install -y notion-snap

echo "*-> Installing VLC... <-*"
apt install -y mpv

echo "*-> Installing qBittorrent... <-*"
apt install -y qbittorrent

echo "*-> Installing Stacer... <-*"
apt install -y stacer

echo "*-> Installing Todoist... <-*"
snap install -y todoist

echo "*-> Installing NitroShare... <-*"
apt-add-repository ppa:george-edison55/nitrosharesudo
​apt updatesudo
​apt install -y nitroshare

echo "*-> Installing Draw.io ... <-*"
snap install -y drawio

echo "*-> Installing BleachBit <-*"
apt install -y bleachbit

#https://www.remnote.io/desktop/linux

echo "*-> Finished installing application packages... <-*"
