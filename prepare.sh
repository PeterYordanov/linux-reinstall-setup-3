#!/bin/bash

apt -y update
apt -y upgrade

apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

apt install -y linux-headers-$(uname -r)

apt install -y snapd

apt install -y --no-install-recommends tzdata
DEBIAN_FRONTEND=noninteractive apt-get install -y keyboard-configuration
