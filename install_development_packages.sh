#!/bin/bash

echo "*-> Installing Essentials... <-*"
apt install -y build-essential
apt install -y cmake make extra-cmake-modules
apt install -y libgl1-mesa-dev wget dirmngr

echo "*-> Installing Git & Vim... <-*"
apt install -y git vim

echo "*-> Download Git Kraken <-*"
curl -O gitkraken-amd64.deb https://release.axocdn.com/linux/gitkraken-amd64.deb

echo "*-> Installing Docker... <-*"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt update -y
apt install -y docker-ce docker-ce-cli containerd.io

echo "*-> Enable Docker... <-*"
groupadd docker
usermod -aG docker $USER
systemctl enable docker.service
systemctl enable containerd.service

echo "*-> Installing Visual Studio Code... <-*"
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | apt-key add -
add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
apt update -y
apt install -y code

echo "*-> Installing Rustup... <-*"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env

echo "*-> Installing QtCreator... <-*"
apt install -y qtcreator

echo "*-> Installing KDevelop... <-*"
apt install -y kdevelop

echo "*-> Setting up git configs... <-*"
git config --global user.email "peter.yordanov.dev@gmail.com"
git config --global user.name "PeterYordanov"

echo "*-> Finished installing dev packages... <-*"
