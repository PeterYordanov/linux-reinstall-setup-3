# Linux Reinstall Setup 3

[[_TOC_]]

## Run Script
```powershell
chmod +x perform.sh
./perform.sh
```

## Preparation
- [x] Install apt-transport-https
- [x] Install ca-certificates
- [x] Install gnupg-agent
- [x] Install software-properties-common
- [x] Install curl

## Install Applications
- [x] Microsoft Edge
- [x] NitroShare
- [x] qBittorrent
- [x] Notion
- [x] KeePassX
- [x] MPV
- [x] Discord
- [x] Firefox
- [x] Stacer
- [x] Todoist
   
## Install Development Apps
- [x] Qt Creator
- [x] KDevelop
- [x] Git
- [x] Git Kraken
- [x] CMake
- [x] Docker
- [x] Rust
- [x] Visual Studio Code
- [x] Vim

## Final Steps
- [x] Configure Git
