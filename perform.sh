#!/bin/bash

chmod +x ./prepare.sh
chmod +x ./install_development_packages.sh
chmod +x ./install_application_packages.sh

./prepare.sh
./install_development_packages.sh
./install_application_packages.sh

apt autoremove
